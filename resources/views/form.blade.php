<!DOCTYPE html>
<html>
<body>

<h2>Grouping Form Data with Fieldset</h2>
<p>The fieldset element is used to group related data in a form, and the legend element defines a caption for the fieldset element.</p>

<form action="/tirage.php">
  <fieldset>
    <legend>Personal information:</legend>
    First name:<br>
    <input type="text" name="firstname" value="Mickey">
    <br>
    Last name:<br>
    <input type="text" name="lastname" value="Mouse">
    <br><br>
    <input type="submit" value="Submit">
	<select name="choix">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
  </select>
  </fieldset>
</form>

</body>
</html>

<script>
function voirParams() {
		var tab = extractionParams();
		alert("nb params = "+size(tab));

		var numParam = 1;
		var s = "";
		for ( var cle in tab) {
			s += "paramètre numéro "+numParam +" : nom= "+cle +" valeur= "+tab[cle];
			s += "<br />";
			numParam++;
		}
		document.getElementById("result").innerHTML = s;
	}

	function extractionParams() {
		var tabAffectations = location.search.substring(1).split('&');
		var tabParams = new Array();
		for (var i = 0; i < tabAffectations.length; i++) {
			var decod = tabAffectations[i].split('=');
			var nomParam = decod[0];
			var valParam = decod[1];
			tabParams[nomParam] = valParam; // tableau associatif JS
		}
		return tabParams;
	}

	// RQ pour un tableau associatif, la propriété length n'a pas de valeur
	// => on crée une fonction qui compte le nb d'elts en parcourant le tableau

	function size(tab) {
		var nbElts = 0;
		for ( var cle in tab) { // <=> foreach
			nbElts++;
 		}
		return nbElts;
	}
</script>
